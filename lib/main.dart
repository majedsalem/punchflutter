import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() async {
  runApp(const MyApp());
}

class Balance {
  final String account;
  final double balance;

  Balance({required this.account, required this.balance});
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Punch wallet',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Punch wallet'),
    );
  }
}
  String dropdownValue = '0x4fcC6c151e766B253D8dB8503dC0E32B67a5266a';
  String web3Message = '';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<List<Balance>> _balances;

  @override
  void initState() {
    super.initState();
    _balances = _getBalances();
  }

  Future<List<Balance>> _getBalances() async {
    final accounts = [
      "aa-alaba-missi-1ruhjovms58dj82p",
      "aa-coffe-nine-27odqnjao99bma21",
      "aa-nebra-arizo-29n4be7r7h8qd83m",
    ];

    final balances = <Balance>[];

    for (final account in accounts) {
      final response = await http.get(
        Uri.parse(
            'https://b7c4-80-217-227-61.ngrok-free.app/balance?fromaccount=$account&env=0'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      );

      final jsonData = json.decode(response.body);
      final balanceValue =
          double.parse(jsonData['result']['maxUnitBalance'] as String);

      balances.add(Balance(account: account, balance: balanceValue));
    }

    return balances;
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 227, 128, 128),
        title: Center(child: Text(widget.title)),
      ),
      body: Container(
        child: Column(
          children: [
            const SizedBox(height: 10),
            for (var i = 0; i < 3; i++)
              FutureBuilder<List<Balance>>(
                future: _balances,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    final balance = snapshot.data![i];
                    return SenderWidget(
                      account: balance.account,
                      balance: balance.balance,
                    );
                  }
                },
              ),
            const SizedBox(height: 25),
            const Text(
              "Blockchain explorer",
              style: TextStyle(fontSize: 25),
            ),
            Container(
              width: 350,
              height: 200,
              child: Text(web3Message),
              decoration: BoxDecoration(
                color: const Color.fromRGBO(51, 54, 70, 1),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(24),
                  topRight: Radius.circular(24),
                  bottomLeft: Radius.circular(24),
                  bottomRight: Radius.circular(24),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SenderWidget extends StatelessWidget {
  final String account;
  final double balance;

  const SenderWidget({Key? key, required this.account, required this.balance});

  Future _sendEuro(String fromAccount) async{
    final res = await http.get(
      Uri.parse(
          'https://b7c4-80-217-227-61.ngrok-free.app/sendOverDfns?fromaccount=$fromAccount&address=$dropdownValue&env=0'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
      },    );

      final jsonData = json.decode(res.body);
      final hash = double.parse(jsonData['result']['maxUnitBalance'] as String);

      _getBlockchainStatus(hash.toString());
      web3Message = jsonData;
  }

//        var response = await httpClient.GetAsync($"https://api.polygonscan.com/api?module=transaction&action=gettxreceiptstatus&txhash={hash}&apikey={token}");

 Future _getBlockchainStatus(String hash) async{
  var token = "37USZIHA73MXHJ67DW399Q1MZUTNREPW23";
    final res = await http.get(
      Uri.parse(
          'https://api.polygonscan.com/api?module=transaction&action=gettxreceiptstatus&txhash=${hash}&apikey=${token}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
      },    );

      final jsonData = json.decode(res.body);
      web3Message = jsonData;
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Column(
            children: [
              const SizedBox(height: 10),
              Container(
                width: 350,
                height: 120,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(51, 54, 70, 1),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(24),
                    topRight: Radius.circular(24),
                    bottomLeft: Radius.circular(24),
                    bottomRight: Radius.circular(24),
                  ),
                ),
                child: Column(
                  children: [
                    const SizedBox(height: 10),
                    Center(
                      child: Text(
                        "Account: $account",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Center(
                      child: Text(
                        "Balance: $balance",
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        const SizedBox(height: 10),
                        Container(
                          width: 255,
                          padding: EdgeInsets.all(5),
                          child: DropdownButtonExample(),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          width: 80,
                          padding: EdgeInsets.all(5),
                          child: ElevatedButton(
                            child: const Text("Send"),
                            onPressed: () => {_sendEuro(account)},
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                              ),
                              backgroundColor: MaterialStateProperty.all(
                                const Color.fromARGB(255, 227, 128, 128),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class DropdownButtonExample extends StatefulWidget {
  const DropdownButtonExample({Key? key});

  @override
  State<DropdownButtonExample> createState() => _DropdownButtonExampleState();
}

class _DropdownButtonExampleState extends State<DropdownButtonExample> {
  final List<String> list = const [
    '0x4fcC6c151e766B253D8dB8503dC0E32B67a5266a',
    '0xDCf5287179152b63aEc27eaa819D56A78300B046',
    '0x00f2DfcB1C78ebC278529Bf52E76eC8fB6F509a9'
  ];

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      dropdownColor: const Color.fromRGBO(51, 54, 70, 1),
      value: dropdownValue,
      isExpanded: true,
      icon: const Icon(Icons.arrow_downward),
      style: const TextStyle(
        color: Color.fromARGB(255, 230, 230, 230),
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownValue = value!;
        });
      },
      items: list.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value.substring(0, 10)),
        );
      }).toList(),
    );
  }
}
