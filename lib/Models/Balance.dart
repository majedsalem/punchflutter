class WebBalance {
  Balance? balance;

  WebBalance({this.balance});

  WebBalance.fromJson(Map<String, dynamic> json) {
    balance =
        json['result'] != null ? new Balance.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.balance != null) {
      data['result'] = this.balance!.toJson();
    }
    return data;
  }
}

class Balance {
  String? id;
  String? assetSymbol;
  String? balance;
  String? maxUnitBalance;

  Balance({this.id, this.assetSymbol, this.balance, this.maxUnitBalance});

  Balance.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    assetSymbol = json['assetSymbol'];
    balance = json['balance'];
    maxUnitBalance = json['maxUnitBalance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['assetSymbol'] = this.assetSymbol;
    data['balance'] = this.balance;
    data['maxUnitBalance'] = this.maxUnitBalance;
    return data;
  }
}
