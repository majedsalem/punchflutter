import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Balance.dart';

abstract class Backend {
  Future<List<String>> getBalance(List<String> accounts);
  Future<String> initPayment(String fromAccount, String toAccount);
}

String url = "https://punch.money/";
int env = 0;
Dio dio = Dio();

class GetDioService implements Backend {
  Future<dynamic> getMethod(
    String url,
    String method,
  ) async {
    dio.options.headers['content-Type'] = 'application/json';
    //dio.options.headers['Authorization'] = 'Bearer $token';

    return await dio
        .get('https://granna-api.azurewebsites.net/api/${url}',
            options: Options(responseType: ResponseType.json, method: method))
        .then((response) {
      log(response.toString());
      return response;
    });
  }

  @override
  Future<List<String>> getBalance(List<String> accounts) async {
    int balance = 0;

    accounts.forEach((element) async {
      var data =
          await dio.get('${url}/balance?fromaccount=${element}&env=${env}').then((response) => {
          });
    });

    throw UnimplementedError();
  }

  @override
  Future<String> initPayment(String fromAccount, String toAccount) async {
    // TODO: implement initPayment
    throw UnimplementedError();
  }
}

class PostDioService {
  Future<dynamic> getMethod(
    String url,
    String method,
  ) async {
    Dio dio = Dio();
    final prefs = await SharedPreferences.getInstance();

    var token = "";

    print('token');

    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers['Authorization'] = 'Bearer $token';

    return await dio
        .get('https://granna-api.azurewebsites.net/api/${url}',
            options: Options(responseType: ResponseType.json, method: method))
        .then((response) {
      log(response.toString());
      return response;
    });
  }
}
